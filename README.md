# pdfSignature

#### 介绍
js处理pdf展示、分页和签章等功能。功能如下图所示：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/092915_5d0aee40_992426.jpeg "seal-img.jpg")

#### 软件架构
使用pdf.js展示pdf，使用fabric.js处理签章的拖动
1. pdf.js
2. fabric.js
3. vue.js
4. axios.js


#### 安装教程

1.  将项目放到web服务器上（apache、nginx、iis）
2.  直接在浏览器运行:[http://127.0.0.1/index.html](http://127.0.0.1/index.html)(如果配置端口，记得在IP后加端口)

#### 使用说明

1. 删除页面单个签章，操作是单击左键选中要删除的签章，点击 **删除签章** 按钮即可删除

#### 参考文档

1.  [使用fabric.js+pdf.js实现简易盖章](https://juejin.cn/post/6859254890460512264)
2.  [vue.js](https://cn.vuejs.org/v2/guide/)
