<?php
class pdf {
    function infos() {
        $data = $_REQUEST['id'];
        $data = [
            'pdf' => 'show.pdf',
            'sign' => 'sign.png',
            'seal' => 'seal.png'
        ];
        echo json_encode(['code' => 200, 'msg' => 'success', 'data' => $data]);
    }

    function getInfo() {
        $data = $_REQUEST;
        echo json_encode(['code' => 200, 'msg' => 'success', 'data' => $data]);
    }
}

$method = $_REQUEST['v'];
$pdf = new pdf;
if($method == 'infos') {
    $pdf->infos();
} else {
    $pdf->getInfo();
}